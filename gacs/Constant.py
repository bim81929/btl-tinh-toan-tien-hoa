class Constant:

    DEFAULT_DEPOT_X = 250
    DEFAULT_DEPOT_Y = 250

    # Chỉnh dựa vào dòng đầu file đầu vào.
    DEFAULT_X = 500
    DEFAULT_Y = 500

    DEFAULT_TIME = 72000
    CROSSOVER_RATE = 0.75
    MUTATION_RATE = 0.1
    POPULATION_SIZE = 250

    ### Pha 1 ###
    ALPHA = 0.5
    N_GENERATIONS_1 = 400
    PMX_RATE = 0.6
    SPX_RATE = 0.4

    CIM_RATE = 0.6
    SWAP_RATE = 0.4

    ### Pha 2 ###
    N_GENERATIONS_2 = 1000

    # Thời gian cho từng cho kỳ
    T = 18000
    DEFAULT_STOP_EPOCH = 50

    def __init__(self):
        super.init()