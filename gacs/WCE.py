from Constant import Constant


class WCE:
    E_MC = 108000
    P_M = 1
    V = 5
    U = 5
    def __init__(self):
        self.xCorr = Constant.DEFAULT_DEPOT_X
        self.yCorr = Constant.DEFAULT_DEPOT_Y
        self.travelEnergy = 0
        self.chargingTimeTotal = 0 # Tổng thời gian sạc cho các sensors ở round hiện tại
